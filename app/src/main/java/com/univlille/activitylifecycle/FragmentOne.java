package com.univlille.activitylifecycle;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

/**
 * Un fragment tout simple...
 */

public class FragmentOne extends Fragment {

    public FragmentOne() {
    }

    public static FragmentOne newInstance() {
        FragmentOne fragment = new FragmentOne();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toast.makeText(this.getContext(), "Fragment : onCreate", Toast.LENGTH_SHORT).show();
        Log.d("cycle_de_vie", "--->Fragment : onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Toast.makeText(this.getContext(), "Fragment : onCreateView", Toast.LENGTH_SHORT).show();
        Log.d("cycle_de_vie", "--->Fragment : onCreateView");
        return inflater.inflate(R.layout.fragment_one, container, false);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Toast.makeText(this.getContext(), "Fragment : onAttach", Toast.LENGTH_SHORT).show();
        Log.d("cycle_de_vie", "--->Fragment : onAttach");
    }


    @Override
    public void onDetach() {
        super.onDetach();
        Toast.makeText(this.getContext(), "Fragment : onDetach", Toast.LENGTH_SHORT).show();
        Log.d("cycle_de_vie", "--->Fragment : onDetach");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Toast.makeText(this.getContext(), "Fragment : onActivityCreated", Toast.LENGTH_SHORT).show();
        Log.d("cycle_de_vie", "--->Fragment : onActivityCreated");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Toast.makeText(this.getContext(), "Fragment : onDestroyView", Toast.LENGTH_SHORT).show();
        Log.d("cycle_de_vie", "--->Fragment : onDestroyView");
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        Toast.makeText(this.getContext(), "Fragment : onViewStateRestored", Toast.LENGTH_SHORT).show();
        Log.d("cycle_de_vie", "--->Fragment : onViewStateRestored");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Toast.makeText(this.getContext(), "Fragment : onViewCreated", Toast.LENGTH_SHORT).show();
        Log.d("cycle_de_vie", "--->Fragment : onViewCreated");
    }

    //    @Override
    public void onResume() {
        super.onResume();
        Toast.makeText(this.getContext(), "Fragment : onResume", Toast.LENGTH_SHORT).show();
        Log.d("cycle_de_vie", "--->Fragment : onResume");
    }

    @Override
    public void onStart() {
        super.onStart();
        Toast.makeText(this.getContext(), "Fragment : onStart", Toast.LENGTH_SHORT).show();
        Log.d("cycle_de_vie", "--->Fragment : onStart");
    }

    @Override
    public void onPause() {
        super.onPause();
        Toast.makeText(this.getContext(), "Fragment : onPause", Toast.LENGTH_SHORT).show();
        Log.d("cycle_de_vie", "--->Fragment : onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Toast.makeText(this.getContext(), "Fragment : onStop", Toast.LENGTH_SHORT).show();
        Log.d("cycle_de_vie", "--->Fragment : onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this.getContext(), "Fragment : onDestroy", Toast.LENGTH_SHORT).show();
        Log.d("cycle_de_vie", "--->Fragment : onDestroy");
    }

    /**
     * méthode qui permet de sauvegarder des données au cas où l'activité serait détruite.
     * Notez qu'on appelle le super() en dernière ligne.
     */
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        Toast.makeText(this.getContext(), "Fragment : onSaveInstanceState", Toast.LENGTH_SHORT).show();
        Log.d("cycle_de_vie", "--->Fragment : onSaveInstanceState");
        super.onSaveInstanceState(outState);
    }

}