package com.univlille.activitylifecycle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Exemple d'une activité avec un fragment statique
 * qui affiche, dans le logcat et dans des toasts, les
 * différents états par lesquels elle passe tout au long de son cycle de vie.
 * Essayer de basculer l'écran, ou encore de passer sur une autre application pendant que
 * celle-ci fonctionne pour voir les pause, destroy, saveInstance, etc....
 */

public class MainActivity extends AppCompatActivity {
    private String maData = "Prems";
    private Button bouton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Toast.makeText(this, "Activité : onCreate", Toast.LENGTH_SHORT).show();
        Log.d("cycle_de_vie", "Activité : onCreate");
        Log.d("cycle_de_vie", "Activité : onCreate, ma data = "+maData);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        findViewById(R.id.btn).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                maData = "JE SUIS NOUVEAU";
                Log.d("cycle_de_vie", "Activité : clic bouton, data = "+ maData);
            }
        });
    }

    @Override
    protected void onResume() {
        Toast.makeText(this, "Activité : onResume", Toast.LENGTH_SHORT).show();
        Log.d("cycle_de_vie", "Activité : onResume");
        Log.d("cycle_de_vie", "Activité : onResume, ma data " + maData);
        Log.d("cycle_de_vie", "Activité : onResume, ma data " + maData);
        super.onResume();
    }

    @Override
    protected void onRestart() {
        Toast.makeText(this, "Activité : onRestart", Toast.LENGTH_SHORT).show();
        Log.d("cycle_de_vie", "Activité : onRestart");
        super.onRestart();
    }

    @Override
    protected void onStart() {
        Toast.makeText(this, "Activité : onStart", Toast.LENGTH_SHORT).show();
        Log.d("cycle_de_vie", "Activité : onStart");
        super.onStart();
    }

    @Override
    protected void onPause() {
        Toast.makeText(this, "Activité : onPause", Toast.LENGTH_SHORT).show();
        Log.d("cycle_de_vie", "Activité : onPause");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Toast.makeText(this, "Activité : onStop", Toast.LENGTH_SHORT).show();
        Log.d("cycle_de_vie", "Activité : onStop");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Toast.makeText(this, "Activité : onDestroy", Toast.LENGTH_SHORT).show();
        Log.d("cycle_de_vie", "Activité : onDestroy");
        super.onDestroy();
    }

    /**
     * méthode qui permet de sauvegarder des données au cas où l'activité serait détruite.
     * Notez qu'on appelle le super() en dernière ligne, après avoir éventuellement sauvegardé
     * des données.
     */
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        Toast.makeText(this, "Activité : onSaveInstanceState", Toast.LENGTH_SHORT).show();
        Log.d("cycle_de_vie", "Activité : onSaveInstanceState");
        // sauvegarde éventuelle de données...
        // ...
        outState.putString("dataSauvee", maData);
        super.onSaveInstanceState(outState);
    }

    /**
     * méthode qui permet de retrouver des données au cas où l'activité serait recréée.
     * Notez qu'on appelle le super() en première ligne avant d'éventuellement récupérer des
     * données.
     */
    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        Toast.makeText(this, "Activité : onRestoreInstanceState", Toast.LENGTH_SHORT).show();
        Log.d("cycle_de_vie", "Activité : onRestoreInstanceState");
        super.onRestoreInstanceState(savedInstanceState);
        // récupération éventuelle de données
        // ...
        maData = savedInstanceState.getString("dataSauvee");
        Log.d("cycle_de_vie", "Activité : onRestoreInstanceState, data = "+maData);

    }
}